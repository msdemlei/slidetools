%
% Slide tools for lecture slides and handouts. 
% Slide tools is extending the LaTeX beamer class.
% 
%
%
% LICENSE
%
% To the extend possible under law, this code is not copyrighted but
% dedicated to the public domain and anybody caught singin' it without our
% permission, will be mighty good friends of ourn, cause we don't give a
% dern. Publish it. Write it. Copy it. Extend it. Sell it. Buy it. Sing it. 
% Swing to it. Yodel it. We wrote it, that's all we wanted to do. 
%
% You should have received a copy of the CC0 legalcode along with this
% work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{slidetools}[2023/11/21 Standard LaTeX Class]


% To satisfy the need of slides and a longer version, we use \myMode
% which is set in Makefile through something like
% pdflatex -jobname=long '\def\myMode{a}\input talk.tex'
% Where a is for articel mode and p for presentation. 
%
% TODO: for HTML it might be necessary to add another condition


\if\myMode p
  \LoadClass{beamer}
  \mode<presentation>
  \usetheme{metropolis}


\else
  \if\myMode n
    \LoadClass[a4paper,11pt]{article}
  \else
    \if\myMode t
      \LoadClass[a4paper,
                 10pt]{article}
      \usepackage[landscape,
                  twocolumn=true,
		  columnsep=2cm]{geometry}
    \else
      \if\myMode e
      \LoadClass[a4paper,
                 10pt]{article}
      \usepackage[landscape,
                  twocolumn=true,
		  columnsep=2cm]{geometry}
      \usepackage[active,
                  generate=exercises,
		  extract-env=Exercise,
		  copydocumentclass=false,
		  handles=false,
		  header=false]{extract}
     \fi
    \fi
  \fi
  \RequirePackage{beamerarticle}
  \mode<article>
  \RequirePackage{fullpage}
  \RequirePackage[colorlinks,
    linkcolor=linkcolor,
    anchorcolor=linkcolor,
    citecolor=linkcolor,
    urlcolor=linkcolor]{hyperref}
\fi

\RequirePackage[color={0.318 0 0.318}]{attachfile2}
\RequirePackage{verbatim}
\RequirePackage{xkeyval}
\RequirePackage{tikz}
\RequirePackage{pifont}


% store PDF figures as .pdfig; we're cleaning up .pdf in our make clean.
% Depends on package{tikz}
\DeclareGraphicsRule{.pdfig}{pdf}{.pdfig}{}

% counting the exercises so we can find them on the solution page
\newcounter{count_exercise}

\newcommand{\exercise}[1]
{\stepcounter{count_exercise} \only<article> {\noindent\textbf{\newline
Exercise \arabic{count_exercise}\newline}#1}}

\newcommand{\question}[1]{\only<article>{\textbf{Problem:} #1\newline}}
\newcommand{\answer}[1]{\only<article>{\textbf{Solution:} #1}}



% The Exercise environment helps to organize Q&As as well as extracting
% them from the document.
% The label argument can link the Exercises to a specific slide. 
% Exercises can have more than one Q&A
% Exercises must be outside the frame environment!

\define@key{Exercise}{label}[]{\def\Exercise@label{#1}}
\define@key{Exercise}{name}[]{\def\Exercise@name{#1}}

\newcounter{Exercise}

\newenvironment{Exercise}[1][]{
\stepcounter{Exercise}
\def\Exercise@label{}
\def\Exercise@name{}
\setkeys{Exercise}{name,label,#1}
\begin{frame}{
\only<article>{Exercise \arabic{Exercise}:}
\only<presentation>{
{\Exercise@name \mbox{}\hfill Exercise
\arabic{Exercise}}
}
}}
{\end{frame}}

% The Question environment should be embedded into an Exercise
% environment. Question takes [label] which per default is empty, and it
% can be referred to in the Answer environment. 
% Per default, Q&As will appear in all modes, with every Q and every A
% on an extra slide. You can skip these appearance with the \only<mode>
% command provided by the beamer class from within the source file of
% the lecture (usually that'll be the file talk.tex).

\define@key{Question}{name}[]{\def\Question@name{#1}}
\define@key{Question}{label}[]{\def\Question@label{#1}}

\newenvironment{Question}[1][]{
\def\Question@label{}
\def\Question@name{}
\setkeys{Question}{name,label,{#1}}
\textbf{Problem:}}
{}

\define@key{Answer}{ref}[]{\def\Answer@ref{#1}}


% The Answer environment possibly takes the [ref] reference argument,
% which links to the label given by an Question environment. 

\newenvironment{Answer}[1][]{
\def\Answer@ref{}
\setkeys{Answer}{ref,#1}

\textbf{Solution:}}
{}


% The Sample environment marks examples anywhere. Per default, samples
% have a counter so it's easy to refer to a specific example. 

\newcounter{Sample}

\newenvironment{Sample}[1][]{
\stepcounter{Sample} \ding{227} \arabic{Sample} }{}


% The Code environment is meant for Code, which in principle would be
% executable with only reasonable extension e.g. adding 
% import necessaryclass AS nc 
% for python code. For short code snippets, use the command snippet
% below.
%
% Code accepts the argument language. You may use it for an output. We
% use it for parsing the wrapped Code and pass it to scripts for
% testing. 

\define@key{Code}{language}[]{\def\Code@language{#1}}
\newenvironment{Code}[1][]{
\begin{semiverbatim}}{\end{semiverbatim}}


\newcommand{\snippet}[1]{\texttt{#1}}


