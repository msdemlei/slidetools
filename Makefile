
SLIDESOPS = -jobname="slides" -pretex="\def\myMode{p}" -usepretex
NOTESOPS = -jobname="notes" -pretex="\def\myMode{n}" -usepretex
TWOOPS = -jobname="twoup" -pretex="\def\myMode{t}" -usepretex
EXTRACT = -jobname="samples" -pretex="\def\myMode{e}" -usepretex


CMD = latexmk -lualatex -pdf 
FILE = talk.tex

all: slides notes twoup

slides: $(FILE)
	$(CMD) $(SLIDESOPS) $(FILE)

notes:  $(FILE) Exercises.tex
	$(CMD) $(NOTESOPS) $(FILE)

twoup:  $(FILE) Exercises.tex
	$(CMD) $(TWOOPS) $(FILE)

Exercises.tex:	$(FILE)
		$(CMD) $(EXTRACT) $(FILE)

clean: 
#	latexmk -C $(FILE) slidetools.cls
# darn. This is needed for now, until a decision is made wether to use a
# TeX-Class or several files instead. The unelegance of the following
# lines actually is a strong point toward not using slidetools.cls
	rm *.pdf *.aux *.fdb_latexmk *.fls *.log *.nav *.out *.snm \
	*.atfi *toc

